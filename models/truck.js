const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('truck', new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true,
  },
  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
}));
