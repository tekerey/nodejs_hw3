const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('load', new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true,
  },
  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
  status: {
    type: String,
    required: true,
  },
  state: {
    type: String,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [{message: String, time: String}],
  created_date: {
    type: String,
    required: true,
  },
}));
