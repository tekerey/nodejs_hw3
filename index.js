const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('config');
const reqLogger = require('./middlewares/requestLogger');
const authMiddleware = require('./middlewares/authMiddleware');

const app = express();
const port = process.env.PORT || 8080;
const dbConfig = config.get('dbConfig');

// routers
const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

mongoose.connect(`mongodb+srv://${dbConfig.username}:${dbConfig.password}@cluster0.qrzyc.mongodb.net/${dbConfig.databaseName}?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

app.use(reqLogger());
app.use(cors());
app.use(express.json());

app.use('/api', authRouter);
app.use(authMiddleware);
app.use('/api', userRouter);
app.use('/api', truckRouter);
app.use('/api', loadRouter);

app.listen(port, () => {
  console.log(`Server is listening on ${port} port.`);
});
