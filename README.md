# NODEJS_HW3
## Requirements
You should have [NodeJS](https://nodejs.org) installed on your local machine to run this project.

Also you can create development configuration for project inside `config` folder or use defaults just for testing.
## How to run
#### 1. Install dependencies
`npm install` 
#### 2. Run app
`npm start`


