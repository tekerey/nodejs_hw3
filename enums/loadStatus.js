module.exports = Object.freeze({
  NEW: 'NEW',
  POSTED: 'POSTED',
  ASSIGNED: 'ASSIGNED',
  SHIPPED: 'SHIPPED',
});
