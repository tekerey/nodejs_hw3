const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const blockDriverOnLoad = require('../middlewares/blockDriverOnLoad');

const {getUser,
  changePassword,
  deleteUser} = require('../controllers/userController');

router.get('/users/me', getUser);
router.delete('/users/me', blockDriverOnLoad, deleteUser);
router.patch('/users/me/password', blockDriverOnLoad, changePassword);

module.exports = router;
