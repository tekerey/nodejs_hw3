const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const shipperOnly = require('../middlewares/shipperRoleMiddleware');
const driverOnly = require('../middlewares/driverRoleMiddleware');

const {getLoads, addLoad,
  getActiveLoad, changeLoadState,
  getLoadById, editLoad,
  deleteLoad, postLoad,
  getShippingInfo} = require('../controllers/loadController');

router.get('/loads', getLoads);
router.post('/loads', shipperOnly, addLoad);
router.get('/loads/active', driverOnly, getActiveLoad);
router.patch('/loads/active/state', driverOnly, changeLoadState);
router.get('/loads/:id', getLoadById);
router.put('/loads/:id', shipperOnly, editLoad);
router.delete('/loads/:id', shipperOnly, deleteLoad);
router.post('/loads/:id/post', shipperOnly, postLoad);
router.get('/loads/:id/shipping_info', shipperOnly, getShippingInfo);

module.exports = router;
