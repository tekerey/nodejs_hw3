const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const driverOnly = require('../middlewares/driverRoleMiddleware');
const blockDriverOnLoad = require('../middlewares/blockDriverOnLoad');

const {getTrucks, createTruck,
  getTruckById, updateTruck,
  deleteTruck, assignTruck} = require('../controllers/truckController');

router.use('/trucks', driverOnly);

router.get('/trucks', getTrucks);
router.post('/trucks', blockDriverOnLoad, createTruck);
router.get('/trucks/:id', getTruckById);
router.put('/trucks/:id', blockDriverOnLoad, updateTruck);
router.delete('/trucks/:id', blockDriverOnLoad, deleteTruck);
router.post('/trucks/:id/assign', blockDriverOnLoad, assignTruck);

module.exports = router;
