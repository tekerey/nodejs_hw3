const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {register, login,
  forgotPassword} = require('../controllers/authController');

router.post('/auth/register', register);
router.post('/auth/login', login);
router.post('/auth/forgot_password', forgotPassword); // work

module.exports = router;
