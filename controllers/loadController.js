const Load = require('../models/load');
const Truck = require('../models/truck');
const Joi = require('joi');
const roles = require('../enums/role');
const loadStatus = require('../enums/loadStatus');
const loadState = require('../enums/loadState');
const truckTypes = require('../enums/truckTypes');

module.exports.getLoads = async (req, res) => {
  const {_id, role} = req.userInfo;

  const schema = Joi.object({
    status: Joi.string()
        .valid(loadStatus.ASSIGNED, loadStatus.NEW,
            loadStatus.POSTED, loadStatus.SHIPPED),
    limit: Joi.number().integer().max(50).default(10).positive(),
    offset: Joi.number().integer().default(0).positive(),
  });
  const {error, value} = schema.validate(req.query);
  if (error) {
    return res.status(400).json({message: error.message});
  }

  const {status, limit, offset} = value;

  let loads = [];
  try {
    if (role === roles.DRIVER) {
      const filter = {assigned_to: _id};
      if (status) filter.status = status;
      loads = await Load.find(filter)
          .skip(offset).limit(limit)
          .select({__v: false}).exec();
    } else if (role === roles.SHIPPER) {
      const filter = {created_by: _id};
      if (status) filter.status = status;
      loads = await Load.find(filter)
          .skip(offset).limit(limit)
          .select({__v: false}).exec();
    }

    res.status(200).json({loads});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.addLoad = async (req, res) => {
  const {_id} = req.userInfo;

  const schema = Joi.object({
    name: Joi.string().required(),
    payload: Joi.number().positive().required(),
    pickup_address: Joi.string().required(),
    delivery_address: Joi.string().required(),
    dimensions: Joi.object({
      width: Joi.number().positive().required(),
      length: Joi.number().positive().required(),
      height: Joi.number().positive().required(),
    }).required(),
  });
  const {error, value} = schema.validate(req.body);
  if (error) {
    return res.status(400).json({message: error.message});
  }

  // eslint-disable-next-line camelcase
  const {pickup_address, delivery_address, dimensions,
    name, payload} = value;

  try {
    await Load.create({
      created_by: _id,
      assigned_to: null,
      status: loadStatus.NEW,
      state: null,
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      logs: [],
      created_date: new Date().toISOString(),
    });
    res.status(200).json({message: 'Load created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.getActiveLoad = async (req, res) => {
  const {_id} = req.userInfo;

  try {
    const load = await Load.findOne({
      assigned_to: _id,
      status: loadStatus.ASSIGNED,
    }).select({__v: false}).exec();
    if (!load) {
      return res.status(400).json({message: 'No active load'});
    }
    res.status(200).json({load});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.changeLoadState = async (req, res) => {
  const userId = req.userInfo._id;
  // check if active load exist
  // change load state depending on current state

  try {
    const load = await Load.findOne({
      assigned_to: userId,
      status: loadStatus.ASSIGNED,
    }).exec();
    if (!load) {
      return res.status(400).json({message: 'No active load'});
    }

    // change state
    if (load.state === loadState.EN_ROUTE_TO_PICK_UP) {
      load.state = loadState.ARRIVED_TO_PICK_UP;
    } else if (load.state === loadState.ARRIVED_TO_PICK_UP) {
      load.state = loadState.EN_ROUTE_TO_DELIVERY;
    } else if (load.state === loadState.EN_ROUTE_TO_DELIVERY) {
      load.state = loadState.ARRIVED_TO_DELIVERY;
      load.status = loadStatus.SHIPPED;
      await Truck.findOneAndUpdate({
        assigned_to: load.assigned_to,
        status: 'OL',
      }, {status: 'IS'}).exec();
    }

    load.logs.push({
      message: `Load state changed to '${load.state}'`,
      time: new Date().toISOString(),
    });
    await load.save();
    res.status(200).json({
      message: `Load state changed to '${load.state}'`,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.getLoadById = async (req, res) => {
  const {role} = req.userInfo;
  const userId = req.userInfo._id;
  const {id} = req.params;

  if (!id) {
    return res.status(400).json({message: 'No id provided.'});
  }

  // ObjectId validation
  const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const idValidation = idSchema.validate(id);
  if (idValidation.error) {
    return res.status(400).json({message: idValidation.error.message});
  }

  let load;
  try {
    if (role === roles.DRIVER) {
      load = await Load.findOne({_id: id, assigned_to: userId})
          .select({__v: false}).exec();
    } else if (role === roles.SHIPPER) {
      load = await Load.findOne({_id: id, created_by: userId})
          .select({__v: false}).exec();
    }

    if (!load) {
      return res.status(400).json({message: 'No load with that id'});
    }
    res.status(200).json({load});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.editLoad = async (req, res) => {
  const userId = req.userInfo._id;
  const {id} = req.params;

  if (!id) {
    return res.status(400).json({message: 'No id provided.'});
  }

  // ObjectId validation
  const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const idValidation = idSchema.validate(id);
  if (idValidation.error) {
    return res.status(400).json({message: idValidation.error.message});
  }

  const schema = Joi.object({
    name: Joi.string().required(),
    payload: Joi.number().positive().required(),
    pickup_address: Joi.string().required(),
    delivery_address: Joi.string().required(),
    dimensions: Joi.object({
      width: Joi.number().positive().required(),
      length: Joi.number().positive().required(),
      height: Joi.number().positive().required(),
    }).required(),
  });
  const {error, value} = schema.validate(req.body);
  if (error) {
    return res.status(400).json({message: error.message});
  }

  // eslint-disable-next-line camelcase
  const {pickup_address, delivery_address, dimensions,
    name, payload} = value;

  try {
    const load = await Load.findOneAndUpdate({
      _id: id, created_by: userId, status: loadStatus.NEW,
    }, {
      name, payload, pickup_address, delivery_address, dimensions,
    }).exec();

    if (!load) {
      return res.status(400).json({
        message: 'No load with that id and status \'NEW\'',
      });
    }

    res.status(200).json({message: 'Load details changed successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.deleteLoad = async (req, res) => {
  const userId = req.userInfo._id;
  const {id} = req.params;
  if (!id) {
    return res.status(400).json({message: 'No id provided'});
  }

  // ObjectId validation
  const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const idValidation = idSchema.validate(id);
  if (idValidation.error) {
    return res.status(400).json({message: idValidation.error.message});
  }

  try {
    const load = await Load.findOneAndDelete({
      _id: id, created_by: userId, status: loadStatus.NEW,
    }).exec();

    if (!load) {
      return res.status(400).json({
        message: 'No load with that id and status \'NEW\'',
      });
    }

    res.status(200).json({message: 'Load deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.postLoad = async (req, res) => {
  const userId = req.userInfo._id;
  const {id} = req.params;
  if (!id) {
    return res.status(400).json({message: 'No id provided'});
  }

  // ObjectId validation
  const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const idValidation = idSchema.validate(id);
  if (idValidation.error) {
    return res.status(400).json({message: idValidation.error.message});
  }

  try {
    const load = await Load.findOne({
      _id: id, created_by: userId, status: loadStatus.NEW,
    }).exec();

    if (!load) {
      return res.status(400).json({
        message: 'No load with that id and status \'NEW\'',
      });
    }
    load.status = loadStatus.POSTED;

    // search for driver (truck)
    let trucks = await Truck.find({
      assigned_to: {$ne: null},
      status: 'IS',
    }).exec();

    trucks = trucks.filter((truck) => {
      let type;
      for (const t in truckTypes) {
        if (truckTypes[t].name === truck.type) {
          type = truckTypes[t];
          break;
        }
      }
      if (!type) return false;
      else if (type.payload >= load.payload &&
        type.dimensions.length >= load.dimensions.length &&
        type.dimensions.width >= load.dimensions.width &&
        type.dimensions.height >= load.dimensions.height) return true;
      else return false;
    });

    trucks.sort((a, b) => {
      if (a.type === truckTypes.SPRINTER.name &&
         b.type === truckTypes.SMALL_STRAIGHT.name) return -1;
      if (a.type === truckTypes.SPRINTER.name &&
         b.type === truckTypes.LARGE_STRAIGHT.name) return -1;
      if (a.type === truckTypes.SPRINTER.name &&
         b.type === truckTypes.SPRINTER.name) return 0;
      if (a.type === truckTypes.SMALL_STRAIGHT.name &&
         b.type === truckTypes.LARGE_STRAIGHT.name) return -1;
      if (a.type === truckTypes.SMALL_STRAIGHT.name &&
         b.type === truckTypes.SPRINTER.name) return 1;
      if (a.type === truckTypes.SMALL_STRAIGHT.name &&
         b.type === truckTypes.SMALL_STRAIGHT.name) return 0;
      if (a.type === truckTypes.LARGE_STRAIGHT.name &&
         b.type === truckTypes.SMALL_STRAIGHT.name) return 1;
      if (a.type === truckTypes.LARGE_STRAIGHT.name &&
         b.type === truckTypes.LARGE_STRAIGHT.name) return 0;
      if (a.type === truckTypes.LARGE_STRAIGHT.name &&
         b.type === truckTypes.SPRINTER.name) return 1;
    });
    const foundTruck = trucks[0];

    if (!foundTruck) {
      load.status = loadStatus.NEW;
      load.logs.push({
        message: 'Load was posted but driver was not found',
        time: new Date().toISOString(),
      });
      await load.save();
      return res.status(200).json({
        message: 'Load posted successfully',
        driver_found: false,
      });
    }

    // If we found available truck
    load.status = loadStatus.ASSIGNED;
    load.assigned_to = foundTruck.assigned_to;
    load.state = loadState.EN_ROUTE_TO_PICK_UP;
    load.logs.push({
      message: `Load assigned to driver with id ${load.assigned_to}`,
      time: new Date().toISOString(),
    });
    foundTruck.status = 'OL';
    await load.save();
    await foundTruck.save();

    res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.getShippingInfo = async (req, res) => {
  const {id} = req.params;
  const userId = req.userInfo._id;
  if (!id) {
    return res.status(400).json({message: 'No id provided'});
  }

  // ObjectId validation
  const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const idValidation = idSchema.validate(id);
  if (idValidation.error) {
    return res.status(400).json({message: idValidation.error.message});
  }

  try {
    const load = await Load.findOne({
      _id: id,
      created_by: userId,
      status: {$in: [loadStatus.ASSIGNED, loadStatus.SHIPPED]},
    }).select({__v: false}).exec();
    if (!load) {
      return res.status(400).json({message: 'No active load with that id'});
    }

    const truck = await Truck.findOne({
      assigned_to: load.assigned_to,
    }).select({__v: false}).exec();
    if (!truck && load.status === loadStatus.ASSIGNED) {
      return res.status(400).json({message: 'Truck was not found'});
    }

    res.status(200).json({load, truck});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
