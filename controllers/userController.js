const User = require('../models/user');
const crypto = require('crypto');
const Joi = require('joi');

module.exports.getUser = async (req, res) => {
  const {_id} = req.userInfo;

  try {
    const user = await User.findById(_id)
        .select({password: false, __v: false}).exec();
    if (!user) {
      return res.status(400).json({message: 'No user with that id.'});
    }
    res.status(200).json({user});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.changePassword = async (req, res) => {
  const {_id} = req.userInfo;

  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/)
        .required(),
    newPassword: Joi.string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/)
        .required()
        .not(Joi.ref('oldPassword')),
  });

  const {error, value} = schema.validate(req.body);
  if (error) {
    return res.status(400).json({message: error.message});
  }
  const {oldPassword, newPassword} = value;

  try {
    const user = await User.findById(_id).exec();
    if (!user) {
      return res.status(400).json({message: 'No user with that id.'});
    }
    // should check old password before changing it
    const hash = crypto.createHash('sha256')
        .update(oldPassword)
        .digest('hex');
    if (user.password != hash) {
      return res.status(400).json({message: 'Wrong old password'});
    }
    // Change password
    user.password = crypto.createHash('sha256')
        .update(newPassword).digest('hex');
    await user.save();
    res.status(200).json({message: 'Password changed successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.deleteUser = async (req, res) => {
  const {_id} = req.userInfo;

  try {
    const user = await User.findByIdAndDelete(_id).exec();
    if (!user) {
      return res.status(400).json({message: 'No user with that id.'});
    }
    res.status(200).json({message: 'Profile deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
