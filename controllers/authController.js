const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const cryptoRandomString = require('crypto-random-string');
const Joi = require('joi');
const User = require('../models/user');
const roles = require('../enums/role');

const secret = require('config').get('secret');

module.exports.register = async (req, res) => {
  // Validation schema
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/)
        .required(),
    role: Joi.string()
        .required(),
  });

  // Validation
  const {error, value} = schema.validate(req.body);
  if (error) {
    return res.status(400).json({message: error.message});
  }
  const {email, password, role} = value;

  if (role !== roles.DRIVER && role !== roles.SHIPPER) {
    return res.status(400).json({message: `No such role as ${role}`});
  }

  // Create user
  try {
    let user = await User.findOne({email}).exec();
    if (user) {
      return res.status(400)
          .json({message: 'User with that email already exist.'});
    }

    user = new User({
      email,
      password: crypto.createHash('sha256').update(password).digest('hex'),
      role,
      created_date: new Date().toISOString(),
    });

    await user.save();
    res.status(200).json({message: 'Profile created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.login = async (req, res) => {
  // Validation schema
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/)
        .required(),
  });

  // Validation
  const {error, value} = schema.validate(req.body);
  if (error) {
    return res.status(400).json({message: error.message});
  }
  const {email, password} = value;

  // Login
  try {
    const user = await User.findOne({email}).exec();
    if (!user) {
      return res.status(400).json({message: 'User not found'});
    }
    if (user.password != crypto.createHash('sha256')
        .update(password).digest('hex')) {
      return res.status(400).json({message: 'Wrong password'});
    }
    res.status(200).json({
      jwt_token: jwt.sign({
        _id: user._id,
        email: user.email,
        created_date: user.created_date,
      }, secret, {expiresIn: '1h'}),
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.forgotPassword = async (req, res) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
  });
  const {error, value} = schema.validate(req.body);
  if (error) {
    return res.status(400).json({message: error.message});
  }
  const {email} = value;

  try {
    const user = await User.findOne({email}).exec();
    if (!user) {
      return res.status(400).json({message: 'User does not exist'});
    }

    // generate new password, set it and send to email
    const newPassword = cryptoRandomString({length: 15});
    user.password = crypto.createHash('sha256')
        .update(newPassword).digest('hex');

    // send email using test sender credentials
    const gmail = require('config').get('email');
    const transport = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: gmail.adress,
        clientId: gmail.clientId,
        clientSecret: gmail.clientSecret,
        refreshToken: gmail.refreshToken,
        accessToken: gmail.accessToken,
      },
    });
    const mailOptions = {
      from: gmail.adress,
      to: user.email,
      subject: 'Forgot password',
      text: `
      Hi, you received this email because you forgot your password.
      
      Your new password is: ${newPassword}
      Don't forget to change it once you are logged in.`,
    };
    await transport.sendMail(mailOptions);
    await user.save();
    res.status(200).json({message: 'New password sent to your email address'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
