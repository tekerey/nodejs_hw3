const Truck = require('../models/truck');
const Joi = require('joi');
const {SPRINTER, SMALL_STRAIGHT,
  LARGE_STRAIGHT} = require('../enums/truckTypes');

module.exports.getTrucks = async (req, res) => {
  const {_id} = req.userInfo;

  try {
    const trucks = await Truck.find({created_by: _id})
        .select({__v: false}).exec();
    res.status(200).json({trucks});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.createTruck = async (req, res) => {
  const schema = Joi.object({
    type: Joi.string()
        .required()
        .valid(SPRINTER.name, SMALL_STRAIGHT.name, LARGE_STRAIGHT.name),
  });
  const {error, value} = schema.validate(req.body);
  if (error) {
    return res.status(400).json({message: error.message});
  }
  const {type} = value;

  try {
    await Truck.create({
      created_by: req.userInfo._id,
      assigned_to: null,
      type,
      status: 'OS',
      created_date: new Date().toISOString(),
    });
    res.status(200).json({message: 'Truck created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.getTruckById = async (req, res) => {
  const {id} = req.params;
  const userId = req.userInfo._id;

  if (!id) {
    return res.status(400).json({message: 'No id provided.'});
  }

  // ObjectId validation
  const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const idValidation = idSchema.validate(id);
  if (idValidation.error) {
    return res.status(400).json({message: idValidation.error.message});
  }

  try {
    const truck = await Truck.findOne({_id: id, created_by: userId})
        .select({__v: false}).exec();
    if (!truck) {
      return res.status(400).json({message: 'No truck with that id.'});
    }
    res.status(200).json({truck});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.updateTruck = async (req, res) => {
  const {id} = req.params;
  const userId = req.userInfo._id;

  if (!id) {
    return res.status(400).json({message: 'No id provided.'});
  }

  // ObjectId validation
  const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const idValidation = idSchema.validate(id);
  if (idValidation.error) {
    return res.status(400).json({message: idValidation.error.message});
  }

  const schema = Joi.object({
    type: Joi.string()
        .required()
        .valid(SPRINTER.name, SMALL_STRAIGHT.name, LARGE_STRAIGHT.name),
  });
  const {error, value} = schema.validate(req.body);
  if (error) {
    return res.status(400).json({message: error.message});
  }
  const {type} = value;

  try {
    const truck = await Truck.findOneAndUpdate(
        {_id: id, created_by: userId, assigned_to: {$ne: userId}},
        {type}).exec();

    if (!truck) {
      return res.status(400).json({message: 'Truck not found'});
    }
    res.status(200).json({message: 'Truck details changed successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.deleteTruck = async (req, res) => {
  const {id} = req.params;
  const userId = req.userInfo._id;

  if (!id) {
    return res.status(400).json({message: 'No id provided.'});
  }

  // ObjectId validation
  const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const idValidation = idSchema.validate(id);
  if (idValidation.error) {
    return res.status(400).json({message: idValidation.error.message});
  }

  try {
    const truck = await Truck.findOneAndDelete({
      _id: id, created_by: userId, assigned_to: {$ne: userId},
    }).exec();
    if (!truck) {
      return res.status(400).json({message: 'Truck not found'});
    }
    res.status(200).json({message: 'Truck deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.assignTruck = async (req, res) => {
  const {id} = req.params;
  const userId = req.userInfo._id;

  if (!id) {
    return res.status(400).json({message: 'No id provided.'});
  }

  // ObjectId validation
  const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const idValidation = idSchema.validate(id);
  if (idValidation.error) {
    return res.status(400).json({message: idValidation.error.message});
  }

  try {
    const truck = await Truck.findOne({_id: id, created_by: userId}).exec();
    if (!truck) {
      return res.status(400).json({message: 'No truck with that id.'});
    }

    await Truck.updateMany({assigned_to: userId}, {
      assigned_to: null, status: 'OS',
    });
    truck.assigned_to = userId;
    truck.status = 'IS';
    await truck.save();

    res.status(200).json({message: 'Truck assigned successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
