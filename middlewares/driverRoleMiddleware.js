const roles = require('../enums/role');

module.exports = (req, res, next) => {
  const {role} = req.userInfo;

  if (role !== roles.DRIVER) {
    return res.status(403).json({message: 'User\'s role is not \'DRIVER\''});
  }
  next();
};
