const Load = require('../models/load');
const loadStatus = require('../enums/loadStatus');
const roles = require('../enums/role');

module.exports = async (req, res, next) => {
  const userId = req.userInfo._id;
  const {role} = req.userInfo;

  if (role !== roles.DRIVER) {
    next();
    return;
  }

  try {
    const load = await Load.findOne({
      assigned_to: userId,
      status: loadStatus.ASSIGNED,
    }).exec();

    if (load) {
      return res.status(400).json({
        message: 'Driver can not do changes while on load',
      });
    }
    next();
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
