const fs = require('fs');
const path = require('path');
const morgan = require('morgan');
/**
 * @return `morgan` middleware for logging requests
 */
module.exports = () => {
  try {
    fs.accessSync(path.join(__dirname, '../', 'logs'));
  } catch (err) {
    fs.mkdirSync(path.join(__dirname, '../', 'logs'), {
      recursive: true,
    });
  }

  const logStream =
  fs.createWriteStream(path.join(__dirname, '../', 'logs', 'requests.log'), {
    flags: 'a',
  });

  return morgan( 'combined', {stream: logStream} );
};
