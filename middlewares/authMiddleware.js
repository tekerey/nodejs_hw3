const secret = require('config').get('secret');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

module.exports = async (req, res, next) => {
  const authHeader = req.headers['authorization'] ||
    req.body.token || req.query.token;

  if (!authHeader) {
    return res.status(401).json({message: 'No authorization header.'});
  }

  // Authorization: {token}
  // Authorization: Bearer {token}
  const parts = authHeader.split(' ');
  const token = parts[1] || parts[0];
  let decoded;
  try {
    decoded = jwt.verify(token, secret);
  } catch (err) {
    return res.status(401).json({message: 'Invalid JWT'});
  }

  try {
    const user = await User.findById(decoded._id).exec();
    if (!user) {
      return res.status(401).json({message: 'User does not exist'});
    }
    req.userInfo = user;
    next();
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
